require 'bundler'
require './ldap.rb'

return false unless init_ldap_connexion(CONFIG)

user = nil
until user
	print "Quelle est l'adresse email ? "
	email = gets.chomp
	user = search_user(email)
	puts "L'adresse email saisie n'est pas valide" unless user
end

puts "========================================================"
puts "Utilisateur : #{user.cn.first} #{user.sn.first}"
puts "========================================================"

if user_domain?(user)
  puts "L'utilisateur fait déjà parti du contrôleur de domaine."
	print "Voulez-vous l'en retirer ? (O/n) "
	disable_domain_user(user) if ['O','o','oui','Oui','OUI', ''].include?(gets.chomp)
else
	puts "L'utilisateur ne fait pas encore parti du contrôleur de domaine."
	print "Voulez vous l'y ajouter ? (O/n) "
	if ['O','o','oui','Oui','OUI',''].include?(gets.chomp)
	  numbers = next_numbers(domain(user.mail.first))
	  enable_domain_user(user, numbers[:uid], numbers[:gid])
	end
end

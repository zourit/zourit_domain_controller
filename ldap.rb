require 'yaml'
require 'net/ldap'

CONFIG = YAML.load_file('./config/ldap.yml')
LDAP_BASE = CONFIG['base']
MIN_UID = MIN_GID = 10000

def init_ldap_connexion(config)
  $ldap = Net::LDAP.new(
    host: config['host'],
    port: config['port'],
    encryption: {
      method: :simple_tls,
      tls_options: { verify_mode: OpenSSL::SSL::VERIFY_NONE }
    },
    auth: { :method => :simple, username: config['admin'], password: config['password'] }
  )
  puts "authenticating to LDAP #{$ldap.host}:#{$ldap.port} as #{config['admin']} ..."
  begin
    $ldap.bind
    puts 'bind LDAP successful'
    return true
  rescue StandardError => e
    puts 'LDAP Connexion failed', e
    return false
  end
end

def domain(email)
  email.split('@').last
end

def get_dn(email)
  dn = "mail=#{email},ou=people,o=#{domain(email)},#{LDAP_BASE}"
end

def search_user(email)
	return false unless email
	
	dn = get_dn(email)
	attributes = %w[db entryuuid sn cn displayname mail userpassword objectclass zouritadmin
		                  zouritsuperadmin zouritzimbraquota zouritcloudquota zourittheme]
	rs = $ldap.search(base: dn, filter: nil, attributes: attributes)
	rs&.first
end

def enable_domain_user(user, uidNumber, gidNumber)
  dn = get_dn(user.mail.first)
  uid = user.mail.first.split('@').first
  new_objectclass = user.objectclass << "posixAccount"
  args = []
  args << [:replace, "objectClass", new_objectclass]
  args << [:replace, "homeDirectory", "/home/#{uid}"]
  args << [:replace, "uid", uid]
  args << [:replace, "uidNumber", uidNumber.to_s]
  args << [:replace, "gidNumber", gidNumber.to_s]
  args << [:replace, "loginShell", "/bin/bash"]
  ldap_modify(dn, args)
end

def user_domain?(user)
  user.objectClass.include?('posixAccount')
end

def disable_domain_user(user)
  dn = get_dn(user.mail.first)
  new_objectclass = user.objectclass - ["posixAccount"]
  args = []
  args << [:replace, "objectClass", new_objectclass]
  args << [:delete, "homeDirectory"]
  args << [:delete, "uid"]
  args << [:delete, "uidNumber"]
  args << [:delete, "gidNumber"]
  args << [:delete, "loginShell"]
  ldap_modify(dn, args)
end

def ldap_modify(dn, args)
  if $ldap.modify dn: dn, operations: args
    puts "modification effectuée"
  else
    puts "modificiation echouée"
    puts "ERROR LDAP update : #{$ldap.get_operation_result.message}"
  end
end

def next_numbers(domain)
  return false unless domain

	dn = "ou=people,o=#{domain},#{LDAP_BASE}"
	attributes = ['uidnumber', 'gidnumber']
	users = $ldap.search(base: dn, filter: nil, attributes: attributes)
	uids = [MIN_UID]
	gids = [MIN_GID]
	users.each do |user|
    begin
      uids << user.uidNumber.first.to_i
      gids << user.gidNumber.first.to_i
    rescue
    end
	end
	{uid: uids.max+1, gid: gids.max+1}
end
